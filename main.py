def analog_filter(values: list, funct=None):
    if funct is None: funct = lambda x: x if bool(x) else None
    for true_item in map(funct, values):
        if true_item is not None: yield true_item


print(list(analog_filter([1, None, False, 0, {}, '', 'asd'])))
print("###########################")
print(list(analog_filter([1, None, False, 0, {}, '', 'asd'], lambda x: str(x)+'.')))